/////////////////////
//// INIT MODULE ////
/////////////////////
var InitModule = (function(Modernizr) {
  'use strict';


  ////////////////////////////
  //// VARIABLES PRIVADAS ////
  ////////////////////////////
  var vars = {},
    methods = {},
    viewToLoad = location.search.substring(1),
    mobileDetection,
    browserDetection,
    add,
    delet,
    list,
    edit,
    checkInputs,
    clearInputs,
    nuevapelicula = $('#nuevapelicula'),
    modalPelicula = $('#agregarPeliculaModal'),
    calendar = $('[data-toggle="datepicker"]'),
    formPelicula = $('#agregarPeliculaModal form'),
    formPeliculaInput = $('#agregarPeliculaModal form input'),
    formPeliculaInputText = $('#agregarPeliculaModal form input[type="text"]'),
    tablalista = $('#tablepeliculas tbody'),
    pelicula,
    accion = "add",
    selected_index = -1,
    tbPeliculas = localStorage.getItem("peliculas"),
    tbPeliculas = JSON.parse(tbPeliculas);

    if(tbPeliculas == null){
      tbPeliculas = [];
    }



  ////////////////////////////
  //// VARIABLES GLOBALES ////
  ////////////////////////////
  vars = {
    isMobile: false,
    firefox: false,
    ie9: false,
    ie10: false,
    ie11: false,
  };





  //////////////////////////
  //// METODOS PRIVADOS ////
  //////////////////////////
  mobileDetection = function () {
    if (window.Detectizr.device.type !== 'desktop' || Modernizr.mq('(max-width: 1200px)')) {
      vars.isMobile = true;
    } else {
      vars.isMobile = false;
    }
  };
  browserDetection = function () {
    if (window.Detectizr.browser.name === 'ie') {
      if (window.Detectizr.browser.major === '9') {
        vars.ie9 = true;
      }
      if (window.Detectizr.browser.major === '10') {
        vars.ie10 = true;
      }
      if (window.Detectizr.browser.major === '11') {
        vars.ie11 = true;
      }
      if (window.Detectizr.browser.major < 9) {
        $('.update-browser').show(0);
        $('.main').hide(0);
      }
    }
  };

  add = function (){
      var pelicula = JSON.stringify({
         id: $ ("#id"). val (),
         nombre: $ ("#nombre"). val (),
         fecha: $ ("#fecha") .val (),
         estado: $ ("#estado"). val ()
       });
      tbPeliculas.push(pelicula);
      localStorage.setItem ("peliculas", JSON.stringify (tbPeliculas));
      alert ("Se guardaron los datos");
      return true;
  }

  edit = function (){
      tbPeliculas[selected_index] = JSON.stringify({
       // id : $("#id").val(),
       nombre : $("#nombre").val(),
       fecha : $("#fecha").val(),
       estado : $("#estado").val()
      });
     localStorage.setItem("peliculas", JSON.stringify(tbPeliculas));
     alert("¿ Seguro que desea Editar ?")
     accion = "add";
     return true;
  }

  delet = function(){
    tbPeliculas.splice(selected_index, 1);
    localStorage.setItem("peliculas", JSON.stringify(tbPeliculas));
    alert("¿ Esta seguro de Eliminar Pelicula ?")
  }

  list = function(){
    console.log(tbPeliculas);
    for(var i in tbPeliculas){
      var peli = JSON.parse(tbPeliculas[i]);
      tablalista.append('<tr>'+
      '<td>'+
      '<span class="custom-checkbox"><input id="checkbox1" type="checkbox" name="options[]" value="1"> <label for="checkbox1"></label></span></td>'+
      '<td>'+i+'</td>'+
      '<td>'+peli.nombre+'</td>'+
      '<td>'+peli.fecha+'</td>'+
      '<td>'+peli.estado+'</td>'+
      '<td><a href="#editEmployeeModal" data-toggle="modal" class="edit" alt="Edit'+i+'">'+
      '<i data-toggle="tooltip" title="Edit" class="material-icons"></i></a><a href="#deleteEmployeeModal" data-toggle="modal" class="delete" alt="Delete'+i+'"><i data-toggle="tooltip" title="Delete" class="material-icons"></i></a></td></tr>');
    }
  }

  clearInputs = function (){
    selected_index = -1;
    formPeliculaInputText.each(function( i,v ) {
      $(v).val('');
      formPeliculaInput.trigger( "keyup" );
    });

  }

  checkInputs = function (obj) {
    var camposRellenados = true;
    obj.find("input").each(function() {
    var $this = $(this);
            if( $this.val().length <= 0 ) {
                camposRellenados = false;
                return false;
            }
    });

    if(camposRellenados == false) {
        return false;
    }
    else {
        return true;
    }
  }




  //////////////////////////
  //// METODOS PUBLICOS ////
  //////////////////////////

  methods.ready = function() {

    //DETECTAR SI ES MOBILE
    mobileDetection();

    //DETECTAR NAVEGADORES
    browserDetection();


    //******************** OSP_CRUD *******************//
    //Calendario
    calendar.datepicker({
      autoHide : true,
      format: 'dd/mm/yyyy'
    });

    //Select2
    $("#estado").select2();

    // Habilitar BOTON ADD
    $('#fecha').on('pick.datepicker', function (e) {
      if (e.date < new Date()) {
        e.preventDefault();
      }
      setTimeout(function(){
        formPeliculaInput.trigger( "keyup" );
      }, 100);

    });

    // add();
    list();

    // Accion ADD PELICULA
    $(document).on('submit','#agregarPeliculaModal form',function(){
        if(accion == "add"){
          return add();
        }else{
          return edit();
        }

    });


    $(document).on('click','#nuevapelicula',function(){
        selected_index = -1;
        accion = 'add';
    });

    // Accion Editar
    $(document).on('click','.edit',function(){
      accion = "edit";
      selected_index = parseInt($(this).attr("alt").replace("Edit", ""));
      var peli = JSON.parse(tbPeliculas[selected_index]);
      // $("#id").val(peli.id);
      $("#nombre").val(peli.nombre);
      $("#fecha").val(peli.fecha);
      $("#estado").val(peli.estado);
      modalPelicula.modal('show');
      $("#nombre").focus();
     });


    modalPelicula.on('shown.bs.modal', function () {
        $('#nombre').focus();
    })

    //LImpiar Inputs
    $(document).on('click','.btn.btn-default',function(){
        clearInputs();
    });

    // Habilitar BOTON ADD ( VALIDAR CAMPOS)
    formPeliculaInput.keyup(function() {
        var check = checkInputs(formPelicula);
        if(check) {
            $(".btn.btn-success").prop("disabled", false);
        }
        else {
            $(".btn.btn-success").prop("disabled", true);
        }
    });

    // Accion Eliminar
    $(document).on('click','.delete',function(){
       selected_index = parseInt($(this).attr("alt").replace("Delete", ""));
       delet();
       location.reload();
    });

  };


  methods.init = function() {
    //BOTONES
    methods.eventsList();

    //IMPEDIR EFECTO ROLLOVER EN MÓVILES
    if (vars.isMobile) {
      $('.hover').removeClass('hover');
    }




  };


  methods.eventsList = function() {
    //FUNCIONES QUE SE DEBEN EJECUTAR EN EL RESIZE
    $(window).on('resize', methods.resizeActions);



  };


  methods.resizeActions = function() {
    mobileDetection();

    browserDetection();
  };






  return {
    methods: methods,
    vars: vars
  };


})(Modernizr);





//CUANDO HA CARGADO EL DOM
$(document).ready(InitModule.methods.ready);
